package fr.bamlab.reactnativeandroidscreenshot;

import android.content.Context;
import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.view.View;
import android.util.Base64;
import android.util.DisplayMetrics;

import java.io.*;
//import Java.io.StringWriter;
import org.apache.commons.io.IOUtils;

import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.Callback;

class ReactNativeAndroidScreenshotModule extends ReactContextBaseJavaModule {
    //private Context context;
    private Activity mActivity = null;

    public ReactNativeAndroidScreenshotModule(ReactApplicationContext reactContext,Activity activity) {
        super(reactContext);
        //this.context = reactContext;
        this.mActivity = activity;
    }

    protected Activity getActivity() {
       return mActivity;
    }

    /**
     * @return the name of this module. This will be the name used to {@code require()} this module
     * from javascript.
     */
    @Override
    public String getName() {
        return "ReactNativeAndroidScreenshot";
    }

    @ReactMethod
    public void reactNativeAndroidScreenshot(Callback onSuccess,Callback onError) {
      //String path = "scree";
      Activity currentActivity = getActivity();
      float density = currentActivity.getResources().getDisplayMetrics().density;
      try {
        View v1 = currentActivity.getWindow().getDecorView().getRootView();
        //v1.destroyDrawingCache();
        v1.setDrawingCacheEnabled(true);
        Bitmap bm = v1.getDrawingCache(true).copy(Bitmap.Config.RGB_565, false);
        v1.destroyDrawingCache();
        int bmWidth = Math.round(bm.getWidth() / density);
        int bmHeight = Math.round(bm.getHeight() / density);
        Bitmap image = Bitmap.createScaledBitmap(bm, bmWidth, bmHeight, false);
        bm.recycle();
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);


        byte[] byteArray = byteArrayOutputStream.toByteArray();
        byteArrayOutputStream.close();
        String encoded = Base64.encodeToString(byteArray, Base64.DEFAULT);
        onSuccess.invoke(encoded);
      } catch (Exception e) {
        onError.invoke(e.toString());
      }
    }
}
