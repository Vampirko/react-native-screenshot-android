import React from 'react-native';

const ReactNativeAndroidScreenshot = React.NativeModules.ReactNativeAndroidScreenshot;

export default  (onSuccess,onError) => {
  onError = onError?onError:function() {};
  return ReactNativeAndroidScreenshot.reactNativeAndroidScreenshot(onSuccess,onError);
}
